import { browser, protractor, by, element, ElementArrayFinder, ElementFinder } from 'protractor';

export class HomePage {
    title: ElementFinder;
    topHeroList: ElementArrayFinder;
    heroSearchInptBx: ElementFinder;
    clearButton: ElementFinder;
    heroesLink:ElementFinder;
    constructor() {
        this.title = element(by.tagName('h1'));
        this.topHeroList = element.all(by.css('app-dashboard > div > a'));
        this.heroSearchInptBx = element(by.tagName('input'));
        this.clearButton = element(by.buttonText('clear'));
        this.heroesLink = element(by.linkText('Heroes'));
    }
    async heroSearch(heroSearch: string) {
        await this.heroSearchInptBx.sendKeys(heroSearch);
        await element(by.tagName('app-hero-search')).element
            (by.tagName('ul li')).element(by.linkText(heroSearch)).click();
    }
}