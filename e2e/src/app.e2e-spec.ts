import { AppPage } from './app.po';
import { browser, logging } from 'protractor';
import { HomePage } from './homepage-po';
import { HeroesPage } from './heroesPage-po';
import {
  protractor,
  by,
  element,
  ElementArrayFinder,
  ElementFinder,
} from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;
  const homePage: HomePage = new HomePage();
  const heroesPage: HeroesPage = new HeroesPage();

  beforeEach(() => {
    page = new AppPage();
    browser.driver.manage().window().maximize();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Tour of Heroes');
  });

  describe('Home Page', () => {
    it('Verify homepage title', async () => {
      expect(await homePage.title.getText()).toEqual('Tour of Heroes');
    });
    it('Verify Top Hero Counts', async () => {
      expect(await homePage.topHeroList.count()).toEqual(4);
    });
    it('Search for an existing Hero', async () => {
      await homePage.heroSearch('Narco');
      expect(await element(by.tagName('app-hero-detail')).element(by.tagName('h2')).getText()).toEqual('NARCO Details');
    });
  });
  describe('Heroes Page', () => {
    it('Add New Hero', async () => {
      await homePage.heroesLink.click();
      const countOfHeroes = await heroesPage.heroList.count();
      await heroesPage.addNewHero('New Hero');
      const countOfHeroesNew = await heroesPage.heroList.count();
      expect(countOfHeroesNew).toEqual(countOfHeroes + 1);
    });
    it('Delete New Hero', async () => {
      await heroesPage.deleteAddedHero('New Hero');
      const countOfHeroes = await heroesPage.heroList.count();
      expect(element.all(by.css('.heroes li')).count()).toEqual(countOfHeroes);
    });
    it('View Hero Details', async () => {
      const id = await heroesPage.deleteBtn.get(0).element(by.tagName('span')).getText();
      console.log('Id is ', id)
      await heroesPage.viewDetails();
      let expectedId = await element(by.tagName('app-hero-detail')).all(by.tagName('div')).get(0)
      .all(by.tagName('div')).get(0).getText();
      expectedId = expectedId.slice(4)
      expect(expectedId).toEqual(id);
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );
  });
});
