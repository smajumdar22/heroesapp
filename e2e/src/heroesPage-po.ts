import {
  browser,
  protractor,
  by,
  element,
  ElementArrayFinder,
  ElementFinder,
} from 'protractor';

export class HeroesPage {
  title: ElementFinder;
  addHeroInputBox: ElementFinder;
  addButton: ElementFinder;
  heroList: ElementArrayFinder;
  deleteBtn: ElementArrayFinder;
  constructor() {
    this.title = element(by.tagName('h2'));
    this.addHeroInputBox = element(by.tagName('input'));
    this.addButton = element(by.buttonText('add'));
    this.heroList = element.all(by.css('.heroes li'));
    this.deleteBtn = element.all(by.css('.heroes li a'));
  }
  async addNewHero(name: string) {
    await this.addHeroInputBox.sendKeys(name);
    await this.addButton.click();
  }
  async deleteAddedHero(name: string) {
    this.deleteBtn.each(async (ele, index) => {
      const val = await ele.getText();
      if (val.includes(name)) {
        await this.heroList.get(index).element(by.buttonText('x')).click();
        await browser.sleep(3000);
      }
    });    
  }
  async viewDetails(){
      await this.heroList.get(0).element(by.tagName('a')).click();
  }
}
